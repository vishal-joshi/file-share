// Import required packages
const path = require('path');
// Restify for creating server
const restify = require('restify');
// Import bot services
const {BotFrameworkAdapter, MemoryStorage, ConversationState, UserState, PrivateConversationState, ActivityTypes} = require("botbuilder");
// Storage Azure Table
const {CosmosDbPartitionedStorage} = require('botbuilder-azure'); 

// importing mainBot
const { MainBot } = require('./bots');
const { MainDispatcher } = require('./dispatcher');

// UserState Identifier
const { UserProfileData, UserProfileId } = require('./StateProperties');

// Logger
const { logIncomingActivity, logOutgoingActivity, errorLogger } = require('./helper/uvoLogger');

// Spell service
const { getCorrectedText } = require('./helper/spell-service');

const { MEasyProxyService, XMLStub, configEngine, uvoLogger } = require('./helper');
const config = require("./config/" + process.env.HOSTED_ENVIRONMENT + "_config.json");


// Read env variables from env file
const ENV_FILE = path.join(__dirname, '.env');
require('dotenv').config({path: ENV_FILE});

// Setting up the Environment
const ENV = process.env.ENV;

// creating bot adapter
const adapter = new BotFrameworkAdapter({
    appId: process.env.MicrosoftAppID,
    appPassword: process.env.MicrosoftAppPassword,
    openIdMetadataL: process.env.openIdMetadataL
});

// Middleware to Send Typing activity
adapter.use(async (context, next) => {
    await context.sendActivity({ type : ActivityTypes.Typing});

    // Calling the next function in queue
    await next();
});

// TODO add spell service middleware


// Define a state store
let conversationState, userState, privateConversationState;

// For Dev storage
// const memoryStorage = new MemoryStorage();

// For  using Cosmos

const cosmosStorage = new CosmosDbPartitionedStorage({
    cosmosDbEndpoint: configEngine.getEnv(config.AzureServices.cosmos.CosmosDbEndpoint),
    authKey: configEngine.getEnv(config.AzureServices.cosmos.CosmosDbAuthKey),
    databaseId: configEngine.getEnv(config.AzureServices.cosmos.CosmosDbDatabaseId),
    containerId: configEngine.getEnv(config.AzureServices.cosmos.CosmosDbContainerId),
    compatibilityMode: false
});

conversationState = new ConversationState(cosmosStorage);
userState = new UserState(cosmosStorage);
privateConversationState = new PrivateConversationState(cosmosStorage); 

// Accessor created for the use of middleware activity logger function
let userDataAccessor = privateConversationState.createProperty(UserProfileId);

// catch-all for errors in bot.
adapter.onTurnError = async (context, error) => {
    let userData = await userDataAccessor.get(context, new UserProfileData());
    console.log(`\n [onTurnError]: ${ error } `);
    await context.sendTraceActivity(
        'OnTurnError Trace',
        `${ error }`,
        'https://www.botframework.com/schemas/error',
        'TurnError'
    );
    await context.sendActivity("I didn’t quite catch you, please try again.");
    errorLogger(context, userData, error.stack);
    await conversationState.delete(context);
};

/**
 * Spell checker middleware
 * @param  {TurnContext} context
 * @param  {} next next handler in queue
 */

// TODO UNDO comment : Did this to test dialogs without LUIS
adapter.use(async (context, next) => {
    
    // Only run when message is beging sent by the user
    if(context.activity.type === ActivityTypes.Message){
        try{
            console.log('Bing spell check initiated at ', new Date());
            context.activity.text = await getCorrectedText(context.activity.text);
            console.log('Bing spell check Completed at ', new Date());

            // console.log(context.activity.text);
        }catch(error){
            console.error(error);
        }
    }

    // Call next in queue
    await next();
});

/**
 * Middleware to log every in and out activities going through Bot
 * @param  {TurnContext} context
 * @param  {} next for calling next Handler in queue
 */
adapter.use(async (context, next) => {

    let userData = await userDataAccessor.get(context, new UserProfileData());

    // Implementation for incoming activities // For Emulator user is checked
    if (context.activity.type !== "conversationUpdate") {
        logIncomingActivity(context, userData);
    }

    // Implementation for Outgoing activities
    let botReply = "";
    context.onSendActivities(
        async (context, activities, nextsend) => {

            activities
                .filter((a) => a.type === "message")
                .forEach(async (activity) => {
                    if (activity.text) {
                        botReply = activity.text;
                    }
                    if (activity.attachments) {
                        botReply = JSON.stringify(activity.attachments);
                    }

                    logOutgoingActivity(context, userData, activity, botReply);
                    
                });
    
            // Allow the sendActivity process to continue.
            await nextsend();
        }
    );

    // Yield control for next function in queue
    await next();
});


// Define main bot

let bot;

// Define main Dispatcher
let mainDispatcher = new MainDispatcher(privateConversationState);

try {

    bot = new MainBot(privateConversationState, conversationState, userState, mainDispatcher);

} catch(err) {
    console.error(`[botInitializationError]: ${ err }`);
    process.exit();
}

// Create HTTP server
let server = restify.createServer();
server.listen(process.env.port || process.env.PORT || 3978, function() {
    console.log(`\n${ server.name } listening to ${ server.url }`);
});

// Listen for user activity and route them.
server.post('/api/messages', (req, res) => {
    adapter.processActivity(req, res, async (turnContext) => {
        await bot.run(turnContext);
    });
});




