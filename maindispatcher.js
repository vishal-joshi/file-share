const { MessageFactory, InputHints } = require('botbuilder');
const { LuisRecognizer } = require('botbuilder-ai');
const { ComponentDialog, DialogSet, DialogTurnStatus, TextPrompt, WaterfallDialog } = require('botbuilder-dialogs');
const { DialogsId } = require('../src/index');
const { OnTurnProperty, UserProfileData, UserProfileId } = require('../StateProperties');
const path = require('path');
const request = require('request');
const ENV_FILE = path.join(__dirname, '.env');
require('dotenv').config({ path: ENV_FILE });

// Dve test
const { MEasyProxyService, XMLStub, configEngine, uvoLogger, IntentApi } = require('../helper');

const config = require("../config/" + process.env.HOSTED_ENVIRONMENT + "_config.json");
// Dve test

// Import Dialogs
const { CafeteriaReport, Payroll, TaxSheetPage, form16, ApproveTimeSheet,
    ApplyLeaveGetPendingLeave, ApplyLeaveExchange, ApplyLeaveGetPendingExpense, ApproveLeaveAndExpense,
    ApplyLeaveAll, ResetStatesDataDialog, Letters, LwpApiDialog, LMSIssueTwo, TimesheetException,
    DenyLeave, TravelApproval, ShiftAllowance, Dashboard, TravelCancel, TravelClose, VariablePay,
    LMSIssuesDialog, TimeSheet, TravelDomestic, AutoFillTimeSheet, TaxInvoice, CashAdvanceApprove,
    PayrollIssues, CancelLeave, SuperAnnuationReport, CashAdvanceCreate, QnA, Care,
    FinancePAFStatus, ApproveDer,
    SubmitDer, SubmitMer, FinancePIStatus, WeeklyOff, EarningRegisterReport, FinanceInvoiceStatus,
    MerApproval, VoucherStatus, VendorSearch, TiBot, OutStandingAmount, BookMySeat  } = require('../dialogs');

const { BotReplies } = require('../setup/BotReplies/development_botreplies');

const MAIN_WATERFALL_DIALOG = 'mainWaterfallDialog';

const luisConfig = { applicationId: configEngine.getEnv(config.AzureServices.Luis.MainDispatcher.LuisAppId), endpointKey: configEngine.getEnv(config.AzureServices.Luis.MainDispatcher.LuisAPIKey), endpoint: configEngine.getEnv(config.AzureServices.Luis.MainDispatcher.LuisAPIHostName), };

const strformat = (...args) => args.shift().replace(/%([jsd])/g, x => x === '%j' ? JSON.stringify(args.shift()) : args.shift());

class MainDispatcher extends ComponentDialog {
    constructor(privateConversationState) {
        super(DialogsId.MainDispatcher);

        //Assign State object
        this.privateConversationState = privateConversationState;

        this.recognizerOptions = {
            apiVersion: 'v3'
        };

        // Luis instance
        this.recognizer = new LuisRecognizer(luisConfig, {
            apiVersion: 'v3'
        });

        this.addDialog(new TextPrompt('TextPrompt'))
            .addDialog(new WaterfallDialog(MAIN_WATERFALL_DIALOG, [
                this.callLuis.bind(this),
                this.finalStep.bind(this)
            ]));

        // Add Sub Dialogs
        this.addDialog(new CafeteriaReport(privateConversationState));
        this.addDialog(new Payroll(privateConversationState));
        this.addDialog(new TaxSheetPage(privateConversationState));
        this.addDialog(new form16(privateConversationState));
        this.addDialog(new ApproveTimeSheet(privateConversationState));
        this.addDialog(new ApplyLeaveGetPendingLeave(privateConversationState));
        this.addDialog(new ApproveLeaveAndExpense(privateConversationState));
        this.addDialog(new ApplyLeaveExchange(privateConversationState));
        this.addDialog(new ApplyLeaveGetPendingExpense(privateConversationState));
        this.addDialog(new ApplyLeaveAll(privateConversationState));
        this.addDialog(new LMSIssuesDialog(privateConversationState));
        this.addDialog(new ResetStatesDataDialog(privateConversationState));
        this.addDialog(new Letters(privateConversationState));
        this.addDialog(new LwpApiDialog(privateConversationState));
        this.addDialog(new LMSIssueTwo(privateConversationState));
        this.addDialog(new TimesheetException(privateConversationState));
        this.addDialog(new DenyLeave(privateConversationState));
        this.addDialog(new TimeSheet(privateConversationState));
        this.addDialog(new TravelApproval(privateConversationState));
        this.addDialog(new ShiftAllowance(privateConversationState));
        this.addDialog(new Dashboard(privateConversationState));
        this.addDialog(new TravelCancel(privateConversationState));
        this.addDialog(new TravelClose(privateConversationState));
        this.addDialog(new VariablePay(privateConversationState));
        this.addDialog(new TravelDomestic(privateConversationState));
        this.addDialog(new AutoFillTimeSheet(privateConversationState));
        this.addDialog(new TaxInvoice(privateConversationState));
        this.addDialog(new CashAdvanceApprove(privateConversationState));
        this.addDialog(new PayrollIssues(privateConversationState));
        this.addDialog(new CancelLeave(privateConversationState));
        this.addDialog(new QnA(privateConversationState));
        this.addDialog(new SuperAnnuationReport(privateConversationState));
        this.addDialog(new CashAdvanceCreate(privateConversationState));
        this.addDialog(new Care(privateConversationState));
        this.addDialog(new FinancePAFStatus(privateConversationState));
        this.addDialog(new ApproveDer(privateConversationState));
        this.addDialog(new SubmitDer(privateConversationState));
        this.addDialog(new SubmitMer(privateConversationState));
        this.addDialog(new FinancePIStatus(privateConversationState));
        this.addDialog(new WeeklyOff(privateConversationState));
        this.addDialog(new EarningRegisterReport(privateConversationState));
        this.addDialog(new FinanceInvoiceStatus(privateConversationState));
        this.addDialog(new MerApproval(privateConversationState));
        this.addDialog(new TiBot(privateConversationState));
        this.addDialog(new VoucherStatus(privateConversationState));
        this.addDialog(new VendorSearch(privateConversationState));
        this.addDialog(new OutStandingAmount(privateConversationState));
        this.addDialog(new BookMySeat(privateConversationState));


        this.initialDialogId = MAIN_WATERFALL_DIALOG;

        this.userPropertyAccessor = this.privateConversationState.createProperty(UserProfileId);

    }

    /**
     * The run method handles the incoming activity (in the form of a TurnContext) and passes it through the dialog system.
     * If no dialog is active, it will start the default dialog.
     * @param {*} turnContext
     * @param {*} accessor
     */
    async run(turnContext, accessor) {
        try {
            const dialogSet = new DialogSet(accessor);
            dialogSet.add(this);
            const dialogContext = await dialogSet.createContext(turnContext);
            const results = await dialogContext.continueDialog();
            if (results && results.status === DialogTurnStatus.empty) {
                await dialogContext.beginDialog(this.id);
            } else {
                console.log('Dialog stack is not empty.');
            }
        } catch (err) {
            console.log('main dispatcher run function catch --------------', err);
            /* 
                Added log on 04 Aug 20
            */
            let userData = await this.userPropertyAccessor.get(turnContext, new UserProfileData());
            await turnContext.sendActivity(`I didn't quiet catch that, please try again.`);
            // console.log('>>>>>>>>>>>>> line 757',await stepContext.continueDialog());
            uvoLogger.errorLogger(turnContext, userData, err);
        }
    }

    async callLuis(stepContext) {

        let userData = await this.userPropertyAccessor.get(stepContext.context, new UserProfileData());

        try {
            /* 
                Key: forLuis => When exiting from LOOP step if User enters query.
            */
            if (stepContext.options && (stepContext.options.interrupt || stepContext.options.forLuis) ) {

                let onTurnProperty = stepContext.options.luisData;

                if (onTurnProperty && onTurnProperty.intent !== '') {
                    switch (onTurnProperty.intent) {
                    case 'Greet':
                        await stepContext.context.sendActivity(strformat(BotReplies.greetings, userData.Name));
                        return await stepContext.endDialog();
                    case 'Help':
                        await stepContext.context.sendActivity(strformat(BotReplies.help, userData.Name));
                        return await stepContext.endDialog();
                    case 'Signoff':
                        let KeyWords = OnTurnProperty.findAllEntities(onTurnProperty.entities, 'KeyWords');
                        if (userData.isSick) {
                            await stepContext.context.sendActivity(BotReplies.apply_leave_signoff_for_sick);
                        } else if (userData.isVacation) {
                            await stepContext.context.sendActivity(BotReplies.aapply_leave_vacation_signoff);
                        } else if (KeyWords.length > 0) {
                            // Do nothing if KeyWords have been triggred
                            await stepContext.context.sendActivity("You may need to be more specific as to what you need to " + KeyWords[0].entityValue[0][0] + ". For e.g. " + KeyWords[0].entityValue[0][0] + " Leave");
                        } else {
                            await stepContext.context.sendActivity(BotReplies.signoff);
                        }
                        return await stepContext.endDialog();
                    case 'Thanks':
                        await stepContext.context.sendActivity(strformat(BotReplies.thanks, userData.Name));
                        return await stepContext.endDialog();
                    case 'Reports_Transactions_Cafeteria':
                        return await stepContext.beginDialog(DialogsId.CafeteriaReport, {
                            luisdata: onTurnProperty,
                            dialogidreset: false,
                        });
                    case 'PYRL_Transactions_Payslip':
                        return await stepContext.beginDialog(DialogsId.Payroll, {
                            luisdata: onTurnProperty,
                            dialogidreset: false,
                        });
                    case 'PYRL_Transactions_Form16':
                        return await stepContext.beginDialog(DialogsId.Form16, {
                            luisdata: onTurnProperty,
                            dialogidreset: false,
                        });
                    case 'PYRL_Transactions_TaxSheet':
                        return await stepContext.beginDialog(DialogsId.TaxsheetPage, {
                            luisdata: onTurnProperty,
                            dialogidreset: false,
                        });
                    case 'LMS_ApplyLeave_Transactions':
                        return await stepContext.beginDialog(DialogsId.ApplyLeaveAll, {
                            luisdata: onTurnProperty,
                            dialogidreset: false,
                        });
                        // TODO: Intent mapping
                        // Pending and expense
                    case 'pendingleave':
                        return await stepContext.beginDialog(DialogsId.GetPendingLeave, {
                            luisdata: onTurnProperty,
                            dialogidreset: false,
                        });
                    case 'PYRL_Transactions_CompLetter':
                        return await stepContext.beginDialog(DialogsId.Letter, {
                            luisdata: onTurnProperty,
                            dialogidreset: false,
                        });
                    case 'LMS_LeaveCmplx_LWP_Issue':
                        return await stepContext.beginDialog(DialogsId.LwpApi, {
                            luisdata: onTurnProperty,
                            dialogidreset: false,
                        });
                    case 'LMS_GiftLeave_Transactions':
                        return await stepContext.beginDialog(DialogsId.LmsIssueTwo, {
                            luisdata: onTurnProperty,
                            dialogidreset: false,
                        });
                    case 'TS_Exception_Issue':
                        return await stepContext.beginDialog(DialogsId.TimesheetException, {
                            luisdata: onTurnProperty,
                            dialogidreset: false,
                        });
                    case 'LMS_DenyLeave_Transactions':
                        return await stepContext.beginDialog(DialogsId.DenyLeave, {
                            luisdata: onTurnProperty,
                            dialogidreset: false,
                        });
                    case 'TRVL_Transactions':
                        return await stepContext.beginDialog(DialogsId.TravelApproval, {
                            luisdata: onTurnProperty,
                            dialogidreset: false,
                        });
    
                        // Shift allowance
                    case 'shiftallowance':
                        return await stepContext.beginDialog(DialogsId.ShiftAllowance, {
                            luisdata: onTurnProperty,
                            dialogidreset: false,
                        });
                    case 'SA_ShiftAlwnceNotPaid_Issue':
                        return await stepContext.beginDialog(DialogsId.ShiftAllowance, {
                            luisdata: onTurnProperty,
                            dialogidreset: true,
                            id: DialogsId.ShiftAllowanceDialog
                        });
                    case 'SA_ShiftAlwnceApply_Issue':
                        return await stepContext.beginDialog(DialogsId.ShiftAllowance, {
                            luisdata: onTurnProperty,
                            dialogidreset: true,
                            id: DialogsId.ShiftAllowanceUnableToApply
                        });
                    case 'SA_ShiftAlwnceNav_Information':
                        return await stepContext.beginDialog(DialogsId.ShiftAllowance, {
                            luisdata: onTurnProperty,
                            dialogidreset: true,
                            id: DialogsId.ShiftAllowanceNav
                        });
    
    
                    case 'EXPN_DashBoardStatus':
                        return await stepContext.beginDialog(DialogsId.Dashboard, {
                            luisdata: onTurnProperty,
                            dialogidreset: false,
                        });
                    case 'TRVL_CancelTransactions':
                        return await stepContext.beginDialog(DialogsId.TravelCancel, {
                            luisdata: onTurnProperty,
                            dialogidreset: false,
                        });
                    case 'TRVL_CloseTransactions':
                        return await stepContext.beginDialog(DialogsId.TravelClose, {
                            luisdata: onTurnProperty,
                            dialogidreset: false,
                        });
                    case 'PYRL_Cmplx_VP_Issue':
                    case 'PYRL_Cmplx_VP_Information':
                        return await stepContext.beginDialog(DialogsId.VariablePay, {
                            luisdata: onTurnProperty,
                            dialogidreset: false,
                        });
                    case 'Reports_Transactions_TaxInvoice':
                        return await stepContext.beginDialog(DialogsId.TaxInvoice, {
                            luisdata: onTurnProperty,
                            dialogidreset: false,
                        } );

                    case 'CADV_ApprovalTransactions':
                        return await stepContext.beginDialog(DialogsId.CashAdvanceApprove, {
                            luisdata: onTurnProperty,
                            dialogidreset: false,
                        });

                    case 'PYRL_Cmplx_PayslipNotGenerated_Issue':
                        return await stepContext.beginDialog(DialogsId.PayrollIssues, {
                            luisdata: onTurnProperty,
                            dialogidreset: true,
                            id: DialogsId.PayrollIssuesDialog
                        });
    
                    case 'PYRL_Cmplx_SalRevNotAffected_Issue':
                        return await stepContext.beginDialog(DialogsId.PayrollIssues, {
                            luisdata: onTurnProperty,
                            dialogidreset: true,
                            id: DialogsId.SalRevNotAffected
                        });
    
                    case 'PYRL_Cmplx_FlexiDeclration_Issue':
                        return await stepContext.beginDialog(DialogsId.PayrollIssues, {
                            luisdata: onTurnProperty,
                            dialogidreset: true,
                            id: DialogsId.FlexiDeclarationIssue
                        });
    
                    case 'PYRL_Cmplx_SalNotCredited_Issue':
                        return await stepContext.beginDialog(DialogsId.PayrollIssues, {
                            luisdata: onTurnProperty,
                            dialogidreset: true,
                            id: DialogsId.SalaryNotCredited
                        });
    
                    case 'PYRL_Cmplx_FixedSal_Issue':
                        return await stepContext.beginDialog(DialogsId.PayrollIssues, {
                            luisdata: onTurnProperty,
                            dialogidreset: true,
                            id: DialogsId.FixedSalaryIssue
                        });
    
                    case 'PYRL_Cmplx_PublicHolidayAllowance_Issue':
                        return await stepContext.beginDialog(DialogsId.PayrollIssues, {
                            luisdata: onTurnProperty,
                            dialogidreset: true,
                            id: DialogsId.PublicHoliday
                        });
    
                    case 'PYRL_Cmplx_NetPay_Issue':
                        return await stepContext.beginDialog(DialogsId.PayrollIssues, {
                            luisdata: onTurnProperty,
                            dialogidreset: true,
                            id: DialogsId.NetPayIssue
                        });
    
                    case 'PYRL_Cmplx_LOP_Issue':
                        return await stepContext.beginDialog(DialogsId.PayrollIssues, {
                            luisdata: onTurnProperty,
                            dialogidreset: true,
                            id: DialogsId.LOPIssue
                        });
    
                    case 'LMS_CancelLeave_Transactions':
                        return await stepContext.beginDialog(DialogsId.CancelLeave, {
                            luisdata: onTurnProperty,
                            dialogidreset: false,
                        });
                    case 'TS_Transactions':
                    case 'TS_FillTimesheet_Issue':
                        return await stepContext.beginDialog(DialogsId.TimeSheet, {
                            luisdata: onTurnProperty,
                            dialogidreset: true,
                            id: DialogsId.getTimesheetData
                        });
                    case 'TS_LocHoliday_Issue':
                        return await stepContext.beginDialog(DialogsId.TimeSheet, {
                            luisdata: onTurnProperty,
                            dialogidreset: true,
                            id: DialogsId.LocationHoliday
                        });
                    case 'TS_WFH_Issue':
                        return await stepContext.beginDialog(DialogsId.TimeSheet, {
                            luisdata: onTurnProperty,
                            dialogidreset: true,
                            id: DialogsId.WorkFromHome
                        });
                    case 'TRVL_CreateTransactions':
                        return await stepContext.beginDialog(DialogsId.TravelDomestic, {
                            luisdata: onTurnProperty,
                            dialogidreset: false
                        });
                    case 'TS_AutoFill_Issue':
                        return await stepContext.beginDialog(DialogsId.AutoFillTimeSheet, {
                            luisdata: onTurnProperty,
                            dialogidreset: false
                        });

                        // Reports
                    case 'Reports_Transactions_SuperAnnution':
                        return await stepContext.beginDialog(DialogsId.SuperAnnuationReport, {
                            luisdata: onTurnProperty,
                            dialogidreset: false
                        });
                    case 'Reports_Transactions_EarningRegister':
                        return await stepContext.beginDialog(DialogsId.EarningRegisterReport, {
                            luisdata: onTurnProperty,
                            dialogidreset: false
                        } );
        
                    // Cash Advance
                    case 'CADV_ApplyTransactions':
                        return await stepContext.beginDialog(DialogsId.CashAdvanceCreate, {
                            luisdata: onTurnProperty,
                            dialogidreset: false
                        });
                    case 'LMS_LeaveCmplx_LeaveEncash_Issue':
                        return await stepContext.beginDialog(DialogsId.LmsIssueDialog, {
                            luisdata: onTurnProperty,
                            dialogidreset: true,
                            id: DialogsId.LeaveEncashmentIssue
                        } );
                    case 'LMS_LeaveCmplx_AdvanceLeave_Issue':
                        return await stepContext.beginDialog(DialogsId.LmsIssueDialog, {
                            luisdata: onTurnProperty,
                            dialogidreset: true,
                            id: DialogsId.AdvanceLeaveIssue
                        } );
                    case 'LMS_LeaveCmplx_ApplyLeave_Issue':
                        return await stepContext.beginDialog(DialogsId.LmsIssueDialog, {
                            luisdata: onTurnProperty,
                            dialogidreset: true,
                            id: DialogsId.ApplyLeaveIssue
                        } );
                    case 'LMS_LeaveCmplx_LeaveBal_Issue':
                        return await stepContext.beginDialog(DialogsId.LmsIssueDialog, {
                            luisdata: onTurnProperty,
                            dialogidreset: true,
                            id: DialogsId.LeaveBalanceIssue
                        } );
                    case 'LMS_ApproveLeave_Transactions':
                    case 'EXPN_ApprovalTransactions':
                        return await stepContext.beginDialog(DialogsId.ApproveExpenseAndLeave, {
                            luisdata: onTurnProperty,
                            dialogidreset: false
                        } );

                    // Weekly Timesheet OFF
                    case 'TS_WeekOff_Issue':
                        return await stepContext.beginDialog(DialogsId.WeeklyOff, {
                            luisdata: onTurnProperty,
                            dialogidreset: true,
                            id: DialogsId.WeeklyOffDialog
                        } );
                    case 'TS_WeekOff_Information':
                        return await stepContext.beginDialog(DialogsId.WeeklyOff, {
                            luisdata: onTurnProperty,
                            dialogidreset: true,
                            id: DialogsId.ProvideWeeklyOff
                        } );

    
                    case 'HRQnaIntent':
                    case 'TIMQnAIntent':
                    case 'IDEASQnAIntent':
                    case 'FinanceQnAIntent':
                    case 'CSQnAIntent':
                        return await stepContext.beginDialog('QnADialog',
                            { luisdata: {
                                entities: []
                            },
                            dialogidreset: false});

                        // Intent API's Begin

                        // LMS Intent API
                    case 'LMS_Leave_MandatoryLeaves_Information':
                        return await stepContext.beginDialog('QnADialog',
                            { luisdata: {
                                entities: []
                            },
                            dialogidreset: false,
                            intentApiName: IntentApi.LMS_Leave_MandatoryLeaves_Information
                            });
    
                    case 'LMS_Leave_LeaveBalance_Information':
                        return await stepContext.beginDialog('QnADialog',
                            { luisdata: {
                                entities: []
                            },
                            dialogidreset: false,
                            intentApiName: IntentApi.LMS_Leave_LeaveBalance_Information});
    
                    case 'LMS_Leave_LeaveHistory_Information':
                        return await stepContext.beginDialog('QnADialog',
                            { luisdata: {
                                entities: []
                            },
                            dialogidreset: false,
                            intentApiName: IntentApi.LMS_Leave_LeaveHistory_Information});
    
                    case 'LMS_ChildAdoption_Entitlement':
                        return await stepContext.beginDialog('QnADialog',
                            { luisdata: {
                                entities: []
                            },
                            dialogidreset: false,
                            intentApiName: IntentApi.LMS_ChildAdoption_Entitlement});
    
                    case 'LMS_Leave_Bereavement_Entitlement':
                        return await stepContext.beginDialog('QnADialog',
                            { luisdata: {
                                entities: []
                            },
                            dialogidreset: false,
                            intentApiName: IntentApi.LMS_Leave_Bereavement_Entitlement});
    
                    case 'LMS_Leave_Hospitaliztion_Entitlement':
                        return await stepContext.beginDialog('QnADialog',
                            { luisdata: {
                                entities: []
                            },
                            dialogidreset: false,
                            intentApiName: IntentApi.LMS_Leave_Hospitaliztion_Entitlement});
    
                    case 'LMS_Leave_OptionalHoliday_Information':
                        return await stepContext.beginDialog('QnADialog',
                            { luisdata: {
                                entities: []
                            },
                            dialogidreset: false,
                            intentApiName: IntentApi.LMS_Leave_OptionalHoliday_Information});
    
                    case 'LMS_Leave_NextOptionalHoliday_Information':
                        return await stepContext.beginDialog('QnADialog',
                            { luisdata: {
                                entities: []
                            },
                            dialogidreset: false,
                            intentApiName: IntentApi.LMS_Leave_NextOptionalHoliday_Information});
    
                    case 'LMS_HowToApplyLeaveVideo':
                        return await stepContext.beginDialog('QnADialog',
                            { luisdata: {
                                entities: []
                            },
                            dialogidreset: false,
                            intentApiName: IntentApi.LMS_HowToApplyLeaveVideo});
                        
                        // Payroll intent API
    
                            
                    case 'PYRL_AmtForHoliday_Information' : 
                        return await stepContext.beginDialog('QnADialog',
                            { luisdata: {
                                entities: []
                            },
                            dialogidreset: false,
                            intentApiName: IntentApi.PYRL_AmtForHoliday_Information
                            });
                    case 'PYRL_CompanyTransport_Entitlement' : 
                        return await stepContext.beginDialog('QnADialog',
                            { luisdata: {
                                entities: []
                            },
                            dialogidreset: false,
                            intentApiName: IntentApi.PYRL_CompanyTransport_Entitlement
                            });
                    case 'PYRL_CarRental_Details' : 
                        return await stepContext.beginDialog('QnADialog',
                            { luisdata: {
                                entities: []
                            },
                            dialogidreset: false,
                            intentApiName: IntentApi.PYRL_CarRental_Details
                            });
                    case 'PYRL_DriverSalary_Entitlement' : 
                        return await stepContext.beginDialog('QnADialog',
                            { luisdata: {
                                entities: []
                            },
                            dialogidreset: false,
                            intentApiName: IntentApi.PYRL_DriverSalary_Entitlement
                            });
                    case 'PYRL_FuelMaintanance_Details' : 
                        return await stepContext.beginDialog('QnADialog',
                            { luisdata: {
                                entities: []
                            },
                            dialogidreset: false,
                            intentApiName: IntentApi.PYRL_FuelMaintanance_Details
                            });
                    case 'PYRL_HealthCheckup' : 
                        return await stepContext.beginDialog('QnADialog',
                            { luisdata: {
                                entities: []
                            },
                            dialogidreset: false,
                            intentApiName: IntentApi.PYRL_HealthCheckup
                            });
                    case 'PYRL_HealthCheckupStatus' : 
                        return await stepContext.beginDialog('QnADialog',
                            { luisdata: {
                                entities: []
                            },
                            dialogidreset: false,
                            intentApiName: IntentApi.PYRL_HealthCheckupStatus
                            });
                    case 'PYRL_LifeInsurance_Entitlement' : 
                        return await stepContext.beginDialog('QnADialog',
                            { luisdata: {
                                entities: []
                            },
                            dialogidreset: false,
                            intentApiName: IntentApi.PYRL_LifeInsurance_Entitlement
                            });
                    case 'PYRL_LoanForHouseRentDeposit' : 
                        return await stepContext.beginDialog('QnADialog',
                            { luisdata: {
                                entities: []
                            },
                            dialogidreset: false,
                            intentApiName: IntentApi.PYRL_LoanForHouseRentDeposit
                            });
                    case 'PYRL_LocalConveyance_Entitlement' : 
                        return await stepContext.beginDialog('QnADialog',
                            { luisdata: {
                                entities: []
                            },
                            dialogidreset: false,
                            intentApiName: IntentApi.PYRL_LocalConveyance_Entitlement
                            });
                    case 'PYRL_LTA_Details' : 
                        return await stepContext.beginDialog('QnADialog',
                            { luisdata: {
                                entities: []
                            },
                            dialogidreset: false,
                            intentApiName: IntentApi.PYRL_LTA_Details
                            });
                    case 'PYRL_Medical_Details' : 
                        return await stepContext.beginDialog('QnADialog',
                            { luisdata: {
                                entities: []
                            },
                            dialogidreset: false,
                            intentApiName: IntentApi.PYRL_Medical_Details
                            });
                    case 'PYRL_MedicalInsurance_Entitlement' : 
                        return await stepContext.beginDialog('QnADialog',
                            { luisdata: {
                                entities: []
                            },
                            dialogidreset: false,
                            intentApiName: IntentApi.PYRL_MedicalInsurance_Entitlement
                            });
                    case 'PYRL_Miscarriage_Entitlement' : 
                        return await stepContext.beginDialog('QnADialog',
                            { luisdata: {
                                entities: []
                            },
                            dialogidreset: false,
                            intentApiName: IntentApi.PYRL_Miscarriage_Entitlement
                            });
                    case 'PYRL_Mobile_Entitlement' : 
                        return await stepContext.beginDialog('QnADialog',
                            { luisdata: {
                                entities: []
                            },
                            dialogidreset: false,
                            intentApiName: IntentApi.PYRL_Mobile_Entitlement
                            });
                    case 'PYRL_MoboMeal_Details' : 
                        return await stepContext.beginDialog('QnADialog',
                            { luisdata: {
                                entities: []
                            },
                            dialogidreset: false,
                            intentApiName: IntentApi.PYRL_MoboMeal_Details
                            });
                    case 'PYRL_PersonalAccidentInsurance_Entitlement' : 
                        return await stepContext.beginDialog('QnADialog',
                            { luisdata: {
                                entities: []
                            },
                            dialogidreset: false,
                            intentApiName: IntentApi.PYRL_PersonalAccidentInsurance_Entitlement
                            });
                    case 'PYRL_Relocation_Entitlement' : 
                        return await stepContext.beginDialog('QnADialog',
                            { luisdata: {
                                entities: []
                            },
                            dialogidreset: false,
                            intentApiName: IntentApi.PYRL_Relocation_Entitlement
                            });
                    case 'PYRL_SalaryAdvance_Entitlement' : 
                        return await stepContext.beginDialog('QnADialog',
                            { luisdata: {
                                entities: []
                            },
                            dialogidreset: false,
                            intentApiName: IntentApi.PYRL_SalaryAdvance_Entitlement
                            });
                    case 'PYRL_SuperAnnuation_Entitlement' : 
                        return await stepContext.beginDialog('QnADialog',
                            { luisdata: {
                                entities: []
                            },
                            dialogidreset: false,
                            intentApiName: IntentApi.PYRL_SuperAnnuation_Entitlement
                            });
                    case 'PYRL_Telephone_Details' : 
                        return await stepContext.beginDialog('QnADialog',
                            { luisdata: {
                                entities: []
                            },
                            dialogidreset: false,
                            intentApiName: IntentApi.PYRL_Telephone_Details
                            });
                    case 'PYRL_HowToAccessCompRevLetter_Video' : 
                        return await stepContext.beginDialog('QnADialog',
                            { luisdata: {
                                entities: []
                            },
                            dialogidreset: false,
                            intentApiName: IntentApi.PYRL_HowToAccessCompRevLetter_Video
                            });
                    case 'PYRL_HowToGetForm16_Video' : 
                        return await stepContext.beginDialog('QnADialog',
                            { luisdata: {
                                entities: []
                            },
                            dialogidreset: false,
                            intentApiName: IntentApi.PYRL_HowToGetForm16_Video
                            });
                    case 'PYRL_HowToITDeclaration_Video' : 
                        return await stepContext.beginDialog('QnADialog',
                            { luisdata: {
                                entities: []
                            },
                            dialogidreset: false,
                            intentApiName: IntentApi.PYRL_HowToITDeclaration_Video
                            });
                    case 'PYRL_OnCallllowance_Details' : 
                        return await stepContext.beginDialog('QnADialog',
                            { luisdata: {
                                entities: []
                            },
                            dialogidreset: false,
                            intentApiName: IntentApi.PYRL_OnCallllowance_Details
                            });
    
                        // Personal deatils intent api
                    case 'PRSNL_ContactNumber_Information' : 
                        return await stepContext.beginDialog('QnADialog',
                            { luisdata: {
                                entities: []
                            },
                            dialogidreset: false,
                            intentApiName: IntentApi.PRSNL_ContactNumber_Information
                            });
                    case 'PRSNL_Country_Information' : 
                        return await stepContext.beginDialog('QnADialog',
                            { luisdata: {
                                entities: []
                            },
                            dialogidreset: false,
                            intentApiName: IntentApi.PRSNL_Country_Information
                            });
                    case 'PRSNL_Designation' : 
                        return await stepContext.beginDialog('QnADialog',
                            { luisdata: {
                                entities: []
                            },
                            dialogidreset: false,
                            intentApiName: IntentApi.PRSNL_Designation
                            });
                    case 'PRSNL_Location_Details' : 
                        return await stepContext.beginDialog('QnADialog',
                            { luisdata: {
                                entities: []
                            },
                            dialogidreset: false,
                            intentApiName: IntentApi.PRSNL_Location_Details
                            });
                    case 'PRSNL_Band_Details' : 
                        return await stepContext.beginDialog('QnADialog',
                            { luisdata: {
                                entities: []
                            },
                            dialogidreset: false,
                            intentApiName: IntentApi.PRSNL_Band_Details
                            });
                    case 'PRSNL_MyName' : 
                        return await stepContext.beginDialog('QnADialog',
                            { luisdata: {
                                entities: []
                            },
                            dialogidreset: false,
                            intentApiName: IntentApi.PRSNL_MyName
                            });
                    case 'PRSNL_HowToActivateUANNumber_Video' : 
                        return await stepContext.beginDialog('QnADialog',
                            { luisdata: {
                                entities: []
                            },
                            dialogidreset: false,
                            intentApiName: IntentApi.PRSNL_HowToActivateUANNumber_Video
                            });
                    case 'PRSNL_HowToChangeFromCurrentRMToNewRM_Video' : 
                        return await stepContext.beginDialog('QnADialog',
                            { luisdata: {
                                entities: []
                            },
                            dialogidreset: false,
                            intentApiName: IntentApi.PRSNL_HowToChangeFromCurrentRMToNewRM_Video
                            });
                    case 'PRSNL_HowToCreateBlogOnMB_Video' : 
                        return await stepContext.beginDialog('QnADialog',
                            { luisdata: {
                                entities: []
                            },
                            dialogidreset: false,
                            intentApiName: IntentApi.PRSNL_HowToCreateBlogOnMB_Video
                            });
                    case 'PRSNL_HowToCreateCommunityOnMB_Video' : 
                        return await stepContext.beginDialog('QnADialog',
                            { luisdata: {
                                entities: []
                            },
                            dialogidreset: false,
                            intentApiName: IntentApi.PRSNL_HowToCreateCommunityOnMB_Video
                            });
                    case 'PRSNL_HowToEditProfileOnMB_Video' : 
                        return await stepContext.beginDialog('QnADialog',
                            { luisdata: {
                                entities: []
                            },
                            dialogidreset: false,
                            intentApiName: IntentApi.PRSNL_HowToEditProfileOnMB_Video
                            });
                    case 'PRSNL_HowToEditBlogOnMB_Video' : 
                        return await stepContext.beginDialog('QnADialog',
                            { luisdata: {
                                entities: []
                            },
                            dialogidreset: false,
                            intentApiName: IntentApi.PRSNL_HowToEditBlogOnMB_Video
                            });
                    case 'PRSNL_HowToEncashKudosPoints_Video' : 
                        return await stepContext.beginDialog('QnADialog',
                            { luisdata: {
                                entities: []
                            },
                            dialogidreset: false,
                            intentApiName: IntentApi.PRSNL_HowToEncashKudosPoints_Video
                            });
                    case 'PRSNL_HowToJoinCommunityMB_Video' : 
                        return await stepContext.beginDialog('QnADialog',
                            { luisdata: {
                                entities: []
                            },
                            dialogidreset: false,
                            intentApiName: IntentApi.PRSNL_HowToJoinCommunityMB_Video
                            });
                    case 'PRSNL_HowToUpdateBankDetailsinPACEHR' : 
                        return await stepContext.beginDialog('QnADialog',
                            { luisdata: {
                                entities: []
                            },
                            dialogidreset: false,
                            intentApiName: IntentApi.PRSNL_HowToUpdateBankDetailsinPACEHR
                            });
                    case 'PRSNL_HowToUpdateBasicPrsnlInfoInMBPlus_Video' : 
                        return await stepContext.beginDialog('QnADialog',
                            { luisdata: {
                                entities: []
                            },
                            dialogidreset: false,
                            intentApiName: IntentApi.PRSNL_HowToUpdateBasicPrsnlInfoInMBPlus_Video
                            });
                    case 'PRSNL_HowToUpdateMobileNumberOnUANPortal_Video' : 
                        return await stepContext.beginDialog('QnADialog',
                            { luisdata: {
                                entities: []
                            },
                            dialogidreset: false,
                            intentApiName: IntentApi.PRSNL_HowToUpdateMobileNumberOnUANPortal_Video
                            });
                    case 'PRSNL_HowToUpdatemyERABankDetails_Video' : 
                        return await stepContext.beginDialog('QnADialog',
                            { luisdata: {
                                entities: []
                            },
                            dialogidreset: false,
                            intentApiName: IntentApi.PRSNL_HowToUpdatemyERABankDetails_Video
                            });
    
                        // Expense intent API
                    case 'EXPN_HowToClaimRelocationAllowance_Video' :  
                        return await stepContext.beginDialog('QnADialog',
                            { luisdata: {
                                entities: []
                            },
                            dialogidreset: false,
                            intentApiName: IntentApi.EXPN_HowToClaimRelocationAllowance_Video
                            });
                    case 'EXPN_HowToModifyExpenseReport_Video' :  
                        return await stepContext.beginDialog('QnADialog',
                            { luisdata: {
                                entities: []
                            },
                            dialogidreset: false,
                            intentApiName: IntentApi.EXPN_HowToModifyExpenseReport_Video
                            });
                    case 'EXPN_HowToRaiseVisaRelatedClaim_Video' :  
                        return await stepContext.beginDialog('QnADialog',
                            { luisdata: {
                                entities: []
                            },
                            dialogidreset: false,
                            intentApiName: IntentApi.EXPN_HowToRaiseVisaRelatedClaim_Video
                            });
                    case 'EXPN_HowToReumburseYourChildSchoolFee_Video' :  
                        return await stepContext.beginDialog('QnADialog',
                            { luisdata: {
                                entities: []
                            },
                            dialogidreset: false,
                            intentApiName: IntentApi.EXPN_HowToReumburseYourChildSchoolFee_Video
                            });
                    case 'EXPN_HowToViewExpenseReport_Video' :  
                        return await stepContext.beginDialog('QnADialog',
                            { luisdata: {
                                entities: []
                            },
                            dialogidreset: false,
                            intentApiName: IntentApi.EXPN_HowToViewExpenseReport_Video
                            });
                    case 'EXPN_HowToSubmitExpsenseClaim_Video' :  
                        return await stepContext.beginDialog('QnADialog',
                            { luisdata: {
                                entities: []
                            },
                            dialogidreset: false,
                            intentApiName: IntentApi.EXPN_HowToSubmitExpsenseClaim_Video
                            });
                    case 'EXPN_HowToPrintExpenseReport_Video' :  
                        return await stepContext.beginDialog('QnADialog',
                            { luisdata: {
                                entities: []
                            },
                            dialogidreset: false,
                            intentApiName: IntentApi.EXPN_HowToPrintExpenseReport_Video
                            });
                    case 'EXPN_HowToEnterModifyExpenseReport_Video' :  
                        return await stepContext.beginDialog('QnADialog',
                            { luisdata: {
                                entities: []
                            },
                            dialogidreset: false,
                            intentApiName: IntentApi.EXPN_HowToEnterModifyExpenseReport_Video
                            });
                    case 'EXPN_HowToDeleteExpenseClaim_Video' :  
                        return await stepContext.beginDialog('QnADialog',
                            { luisdata: {
                                entities: []
                            },
                            dialogidreset: false,
                            intentApiName: IntentApi.EXPN_HowToDeleteExpenseClaim_Video
                            });
    
                        // Cash advance video
                    case 'CADV_HowToEnterModifyCashAdvance_Video' :   
                        return await stepContext.beginDialog('QnADialog',
                            { luisdata: {
                                entities: []
                            },
                            dialogidreset: false,
                            intentApiName: IntentApi.CADV_HowToEnterModifyCashAdvance_Video
                            });
                    case 'CADV_HowToRaiseReqForBusinessTravelCashAdv_Video' :   
                        return await stepContext.beginDialog('QnADialog',
                            { luisdata: {
                                entities: []
                            },
                            dialogidreset: false,
                            intentApiName: IntentApi.CADV_HowToRaiseReqForBusinessTravelCashAdv_Video
                            });
                    case 'CADV_HowToViewCashAdvanceDetails_Video' :   
                        return await stepContext.beginDialog('QnADialog',
                            { luisdata: {
                                entities: []
                            },
                            dialogidreset: false,
                            intentApiName: IntentApi.CADV_HowToViewCashAdvanceDetails_Video
                            });
    
                        // Shift allowance API
                    case 'SA_GetShiftAllowance_Details': 
                        return await stepContext.beginDialog('QnADialog',
                            { luisdata: {
                                entities: []
                            },
                            dialogidreset: false,
                            intentApiName: IntentApi.SA_GetShiftAllowance_Details
                            });
                        
                        // Travel API
                    case 'TRVL_HowToReopenTravelRequest_Video': 
                        return await stepContext.beginDialog('QnADialog',
                            { luisdata: {
                                entities: []
                            },
                            dialogidreset: false,
                            intentApiName: IntentApi.TRVL_HowToReopenTravelRequest_Video
                            });
                        // CS LUIS API
                    case 'CS_HowToApplyForParkingSticker_Video' : 
                        return await stepContext.beginDialog('QnADialog',
                            { luisdata: {
                                entities: []
                            },
                            dialogidreset: false,
                            intentApiName: IntentApi.CS_HowToApplyForParkingSticker_Video
                            });
                    case 'Medi_HowToGetMedibuddyMedicalCard_Video' : 
                        return await stepContext.beginDialog('QnADialog',
                            { luisdata: {
                                entities: []
                            },
                            dialogidreset: false,
                            intentApiName: IntentApi.Medi_HowToGetMedibuddyMedicalCard_Video
                            });
                    case 'Medi_HowToLoginIntoMedibuddyPortal_Video' : 
                        return await stepContext.beginDialog('QnADialog',
                            { luisdata: {
                                entities: []
                            },
                            dialogidreset: false,
                            intentApiName: IntentApi.Medi_HowToLoginIntoMedibuddyPortal_Video
                            });
    
    
                        // Intent API's END
                    case 'CARE_Transactions_WorkHarassment':
                    case 'CARE_other':
                    case 'CARE_Transactions_InterpersonalIssue':
                    case 'CARE_Transactions_PolicyViolation':
                        return await stepContext.beginDialog(DialogsId.CARE, {
                            luisdata: onTurnProperty,
                            dialogidreset: false
                        } );

                    // Finance / Procurement
                    case 'PROC_PafStatus':
                        return await stepContext.beginDialog(DialogsId.FinancePAFStatus, {
                            luisdata: onTurnProperty,
                            dialogidreset: false
                        } );
                    // Pistatus
                    case 'PROC_Pistatus':
                        return await stepContext.beginDialog(DialogsId.FinancePIStatus,{
                            luisdata: onTurnProperty,
                            dialogidreset: false
                        });
                    // Finance_venderSeacrh
                    case 'PROC_VendorSearch':
                        return await stepContext.beginDialog(DialogsId.VendorSearch, {
                            luisdata: onTurnProperty,
                            dialogidreset: false
                        } );
    

                    // Invoice status
                    case 'PROC_InvoiceStatus':
                        return await stepContext.beginDialog(DialogsId.FinanceInvoiceStatus,{
                            luisdata: onTurnProperty,
                            dialogidreset: false
                        });

                    // Proc Outstanding
                    case 'OutstandingBalance':
                    case 'PROC_OutstandingBalance':
                        return await stepContext.beginDialog(DialogsId.OutStandingAmount, {
                            luisdata: onTurnProperty,
                            dialogidreset: false
                        });


                    // DER Modules
                    case 'DER_Submit_Transactions':
                        return await stepContext.beginDialog(DialogsId.SubmitDer,{
                            luisdata: onTurnProperty,
                            dialogidreset: false
                        });
                    case 'DER_Transactions':
                        return await stepContext.beginDialog(DialogsId.ApproveDer,{
                            luisdata: onTurnProperty,
                            dialogidreset: false
                        });

                    // MER Modules
                    case 'MER_Transactions':
                        return await stepContext.beginDialog(DialogsId.MerApproval,{
                            luisdata: onTurnProperty,
                            dialogidreset: false
                        });
                        //TODO
                        
                    case 'MER_Submit_Transactions':
                        return await stepContext.beginDialog(DialogsId.SubmitMer,{
                            luisdata: onTurnProperty,
                            dialogidreset: false
                        });

                    // Voucher status                    
                    case 'PROC_VoucherStatus':
                        return await stepContext.beginDialog(DialogsId.VoucherStatus,{
                            luisdata: onTurnProperty,
                            dialogidreset: false
                        });
                    case 'TS_UnableToApproveTimesheet_Issue':
                        return await stepContext.beginDialog(DialogsId.ApproveTimeSheet, {
                            luisdata: onTurnProperty,
                            dialogidreset: false
                        });
                    // Book my seat
                    case 'CS_BookMySeat':
                        return await stepContext.beginDialog(DialogsId.BookMySeat,{
                            luisdata: onTurnProperty,
                            dialogidreset: false
                        });
                     
                        
                    default:
                        this.fallback = true;
                        return await stepContext.beginDialog('QnADialog',
                            { luisdata: {
                                entities: []
                            },
                            dialogidreset: true,
                            id: 'QnADefault'
                            });
                    }  
                } else {
                    uvoLogger.logFailedUttToDB(stepContext.context, userData);
                    switch (stepContext.context.activity.text) {
                    default:
                        this.fallback = true;
                        return await stepContext.beginDialog('QnADialog',
                            { luisdata: {
                                entities: []
                            },
                            dialogidreset: true,
                            id: 'QnADefault'
                            });
                    }
                }
            } else {
                let onTurnProperty = await LuisData(this.recognizer, stepContext);

                if (onTurnProperty && onTurnProperty.intent) {
                    uvoLogger.logIntentToDB(stepContext.context, userData, onTurnProperty.intent );
                    switch (onTurnProperty.intent) {
                    case 'Greet':
                        await stepContext.context.sendActivity(strformat(BotReplies.greetings, userData.Name));
                        return await stepContext.endDialog();
                    case 'Help':
                        await stepContext.context.sendActivity(strformat(BotReplies.help, userData.Name));
                        return await stepContext.endDialog();
                    case 'Signoff':
                        let KeyWords = OnTurnProperty.findAllEntities(onTurnProperty.entities, 'KeyWords');
                        if (userData.isSick) {
                            await stepContext.context.sendActivity(BotReplies.apply_leave_signoff_for_sick);
                        } else if (userData.isVacation) {
                            await stepContext.context.sendActivity(BotReplies.aapply_leave_vacation_signoff);
                        } else if (KeyWords.length > 0) {
                            // Do nothing if KeyWords have been triggred
                            await stepContext.context.sendActivity("You may need to be more specific as to what you need to " + KeyWords[0].entityValue[0][0] + ". For e.g. " + KeyWords[0].entityValue[0][0] + " Leave");
                        } else {
                            await stepContext.context.sendActivity(BotReplies.signoff);
                        }
                        return await stepContext.endDialog();
                    case 'Thanks':
                        await stepContext.context.sendActivity(strformat(BotReplies.thanks, userData.Name));
                        return await stepContext.endDialog();
                    case 'Reports_Transactions_Cafeteria':
                        return await stepContext.beginDialog(DialogsId.CafeteriaReport, {
                            luisdata: onTurnProperty,
                            dialogidreset: false,
                        });
                    case 'PYRL_Transactions_Payslip':
                        return await stepContext.beginDialog(DialogsId.Payroll, {
                            luisdata: onTurnProperty,
                            dialogidreset: false,
                        });
                    case 'PYRL_Transactions_Form16':
                        return await stepContext.beginDialog(DialogsId.Form16, {
                            luisdata: onTurnProperty,
                            dialogidreset: false,
                        });
                    case 'PYRL_Transactions_TaxSheet':
                        return await stepContext.beginDialog(DialogsId.TaxsheetPage, {
                            luisdata: onTurnProperty,
                            dialogidreset: false,
                        });
                    case 'LMS_ApplyLeave_Transactions':
                        return await stepContext.beginDialog(DialogsId.ApplyLeaveAll, {
                            luisdata: onTurnProperty,
                            dialogidreset: false,
                        });
                        // TODO: Intent mapping
                        // Pending and expense
                    case 'pendingleave':
                        return await stepContext.beginDialog(DialogsId.GetPendingLeave, {
                            luisdata: onTurnProperty,
                            dialogidreset: false,
                        });
                    case 'PYRL_Transactions_CompLetter':
                        return await stepContext.beginDialog(DialogsId.Letter, {
                            luisdata: onTurnProperty,
                            dialogidreset: false,
                        });
                    case 'LMS_LeaveCmplx_LWP_Issue':
                        return await stepContext.beginDialog(DialogsId.LwpApi, {
                            luisdata: onTurnProperty,
                            dialogidreset: false,
                        });
                    case 'LMS_GiftLeave_Transactions':
                        return await stepContext.beginDialog(DialogsId.LmsIssueTwo, {
                            luisdata: onTurnProperty,
                            dialogidreset: false,
                        });
                    case 'TS_Exception_Issue':
                        return await stepContext.beginDialog(DialogsId.TimesheetException, {
                            luisdata: onTurnProperty,
                            dialogidreset: false,
                        });
                    case 'LMS_DenyLeave_Transactions':
                        return await stepContext.beginDialog(DialogsId.DenyLeave, {
                            luisdata: onTurnProperty,
                            dialogidreset: false,
                        });
                    case 'TRVL_Transactions':
                        return await stepContext.beginDialog(DialogsId.TravelApproval, {
                            luisdata: onTurnProperty,
                            dialogidreset: false,
                        });

                        // Shift allowance
                    case 'shiftallowance':
                        return await stepContext.beginDialog(DialogsId.ShiftAllowance, {
                            luisdata: onTurnProperty,
                            dialogidreset: false,
                        });
                    case 'SA_ShiftAlwnceNotPaid_Issue':
                        return await stepContext.beginDialog(DialogsId.ShiftAllowance, {
                            luisdata: onTurnProperty,
                            dialogidreset: true,
                            id: DialogsId.ShiftAllowanceDialog
                        });
                    case 'SA_ShiftAlwnceApply_Issue':
                        return await stepContext.beginDialog(DialogsId.ShiftAllowance, {
                            luisdata: onTurnProperty,
                            dialogidreset: true,
                            id: DialogsId.ShiftAllowanceUnableToApply
                        });
                    case 'SA_ShiftAlwnceNav_Information':
                        return await stepContext.beginDialog(DialogsId.ShiftAllowance, {
                            luisdata: onTurnProperty,
                            dialogidreset: true,
                            id: DialogsId.ShiftAllowanceNav
                        });


                    case 'EXPN_DashBoardStatus':
                        return await stepContext.beginDialog(DialogsId.Dashboard, {
                            luisdata: onTurnProperty,
                            dialogidreset: false,
                        });
                    case 'TRVL_CancelTransactions':
                        return await stepContext.beginDialog(DialogsId.TravelCancel, {
                            luisdata: onTurnProperty,
                            dialogidreset: false,
                        });
                    case 'TRVL_CloseTransactions':
                        return await stepContext.beginDialog(DialogsId.TravelClose, {
                            luisdata: onTurnProperty,
                            dialogidreset: false,
                        });
                    case 'PYRL_Cmplx_VP_Issue':
                    case 'PYRL_Cmplx_VP_Information':
                        return await stepContext.beginDialog(DialogsId.VariablePay, {
                            luisdata: onTurnProperty,
                            dialogidreset: false,
                        });
                    case 'Reports_Transactions_TaxInvoice':
                        return await stepContext.beginDialog(DialogsId.TaxInvoice, {
                            luisdata: onTurnProperty,
                            dialogidreset: false,
                        } );
                    case 'CADV_ApprovalTransactions':
                        return await stepContext.beginDialog(DialogsId.CashAdvanceApprove, {
                            luisdata: onTurnProperty,
                            dialogidreset: false,
                        });

                    case 'PYRL_Cmplx_PayslipNotGenerated_Issue':
                        return await stepContext.beginDialog(DialogsId.PayrollIssues, {
                            luisdata: onTurnProperty,
                            dialogidreset: true,
                            id: DialogsId.PayrollIssuesDialog
                        });

                    case 'PYRL_Cmplx_SalRevNotAffected_Issue':
                        return await stepContext.beginDialog(DialogsId.PayrollIssues, {
                            luisdata: onTurnProperty,
                            dialogidreset: true,
                            id: DialogsId.SalRevNotAffected
                        });

                    case 'PYRL_Cmplx_FlexiDeclration_Issue':
                        return await stepContext.beginDialog(DialogsId.PayrollIssues, {
                            luisdata: onTurnProperty,
                            dialogidreset: true,
                            id: DialogsId.FlexiDeclarationIssue
                        });

                    case 'PYRL_Cmplx_SalNotCredited_Issue':
                        return await stepContext.beginDialog(DialogsId.PayrollIssues, {
                            luisdata: onTurnProperty,
                            dialogidreset: true,
                            id: DialogsId.SalaryNotCredited
                        });

                    case 'PYRL_Cmplx_FixedSal_Issue':
                        return await stepContext.beginDialog(DialogsId.PayrollIssues, {
                            luisdata: onTurnProperty,
                            dialogidreset: true,
                            id: DialogsId.FixedSalaryIssue
                        });

                    case 'PYRL_Cmplx_PublicHolidayAllowance_Issue':
                        return await stepContext.beginDialog(DialogsId.PayrollIssues, {
                            luisdata: onTurnProperty,
                            dialogidreset: true,
                            id: DialogsId.PublicHoliday
                        });

                    case 'PYRL_Cmplx_NetPay_Issue':
                        return await stepContext.beginDialog(DialogsId.PayrollIssues, {
                            luisdata: onTurnProperty,
                            dialogidreset: true,
                            id: DialogsId.NetPayIssue
                        });

                    case 'PYRL_Cmplx_LOP_Issue':
                        return await stepContext.beginDialog(DialogsId.PayrollIssues, {
                            luisdata: onTurnProperty,
                            dialogidreset: true,
                            id: DialogsId.LOPIssue
                        });

                    case 'LMS_CancelLeave_Transactions':
                        return await stepContext.beginDialog(DialogsId.CancelLeave, {
                            luisdata: onTurnProperty,
                            dialogidreset: false,
                        });
                    case 'TS_Transactions':
                    case 'TS_FillTimesheet_Issue':
                        return await stepContext.beginDialog(DialogsId.TimeSheet, {
                            luisdata: onTurnProperty,
                            dialogidreset: true,
                            id: DialogsId.getTimesheetData
                        });
                    case 'TS_LocHoliday_Issue':
                        return await stepContext.beginDialog(DialogsId.TimeSheet, {
                            luisdata: onTurnProperty,
                            dialogidreset: true,
                            id: DialogsId.LocationHoliday
                        });
                    case 'TS_WFH_Issue':
                        return await stepContext.beginDialog(DialogsId.TimeSheet, {
                            luisdata: onTurnProperty,
                            dialogidreset: true,
                            id: DialogsId.WorkFromHome
                        });
                    case 'TRVL_CreateTransactions':
                        return await stepContext.beginDialog(DialogsId.TravelDomestic, {
                            luisdata: onTurnProperty,
                            dialogidreset: false
                        });
                    case 'TS_AutoFill_Issue':
                        return await stepContext.beginDialog(DialogsId.AutoFillTimeSheet, {
                            luisdata: onTurnProperty,
                            dialogidreset: false
                        });
                    case 'TS_UnableToApproveTimesheet_Issue':
                        return await stepContext.beginDialog(DialogsId.ApproveTimeSheet, {
                            luisdata: onTurnProperty,
                            dialogidreset: false
                        });


                    // Reports Modules
                    // SuperAnnuation
                    case 'Reports_Transactions_SuperAnnution':
                        return await stepContext.beginDialog(DialogsId.SuperAnnuationReport, {
                            luisdata: onTurnProperty,
                            dialogidreset: false
                        });
                        
                    case 'Reports_Transactions_EarningRegister':
                        return await stepContext.beginDialog(DialogsId.EarningRegisterReport, {
                            luisdata: onTurnProperty,
                            dialogidreset: false
                        } );
                    
                    // Cash Advance
                    case 'CADV_ApplyTransactions':
                        return await stepContext.beginDialog(DialogsId.CashAdvanceCreate, {
                            luisdata: onTurnProperty,
                            dialogidreset: false
                        });
                    case 'LMS_LeaveCmplx_LeaveEncash_Issue':
                        console.log('found LMS_LeaveCmplx_LeaveEncash_Issue');
                        return await stepContext.beginDialog(DialogsId.LmsIssueDialog, {
                            luisdata: onTurnProperty,
                            dialogidreset: true,
                            id: DialogsId.LeaveEncashmentIssue
                        } );
                    case 'LMS_LeaveCmplx_AdvanceLeave_Issue':
                        return await stepContext.beginDialog(DialogsId.LmsIssueDialog, {
                            luisdata: onTurnProperty,
                            dialogidreset: true,
                            id: DialogsId.AdvanceLeaveIssue
                        } );
                    case 'LMS_LeaveCmplx_ApplyLeave_Issue':
                        return await stepContext.beginDialog(DialogsId.LmsIssueDialog, {
                            luisdata: onTurnProperty,
                            dialogidreset: true,
                            id: DialogsId.ApplyLeaveIssue
                        } );
                    case 'LMS_LeaveCmplx_LeaveBal_Issue':
                        return await stepContext.beginDialog(DialogsId.LmsIssueDialog, {
                            luisdata: onTurnProperty,
                            dialogidreset: true,
                            id: DialogsId.LeaveBalanceIssue
                        } );
                    case 'LMS_ApproveLeave_Transactions':
                    case 'EXPN_ApprovalTransactions':
                        return await stepContext.beginDialog(DialogsId.ApproveExpenseAndLeave, {
                            luisdata: onTurnProperty,
                            dialogidreset: false
                        } );
                            
                    // Finance / Procurement
                    case 'PROC_PafStatus':
                        return await stepContext.beginDialog(DialogsId.FinancePAFStatus, {
                            luisdata: onTurnProperty,
                            dialogidreset: false
                        } );

                    // Pistatus
                    case 'PROC_Pistatus':
                        return await stepContext.beginDialog(DialogsId.FinancePIStatus,{
                            luisdata: onTurnProperty,
                            dialogidreset: false
                        });

                    // Invoice status
                    case 'PROC_InvoiceStatus':
                        return await stepContext.beginDialog(DialogsId.FinanceInvoiceStatus,{
                            luisdata: onTurnProperty,
                            dialogidreset: false
                        }); 

                    // Finance_venderSeacrh
                    case 'PROC_VendorSearch':
                        return await stepContext.beginDialog(DialogsId.VendorSearch, {
                            luisdata: onTurnProperty,
                            dialogidreset: false
                        } );
                    
                    // Proc Outstanding
                    case 'OutstandingBalance':
                    case 'PROC_OutstandingBalance':
                        return await stepContext.beginDialog(DialogsId.OutStandingAmount, {
                            luisdata: onTurnProperty,
                            dialogidreset: false
                        });

                    // Voucher status                    
                    case 'PROC_VoucherStatus':
                        return await stepContext.beginDialog(DialogsId.VoucherStatus,{
                            luisdata: onTurnProperty,
                            dialogidreset: false
                        }); 
                        
                    // Weekly Timesheet OFF
                    case 'TS_WeekOff_Issue':
                        return await stepContext.beginDialog(DialogsId.WeeklyOff, {
                            luisdata: onTurnProperty,
                            dialogidreset: true,
                            id: DialogsId.WeeklyOffDialog
                        } );
                    case 'TS_WeekOff_Information':
                        return await stepContext.beginDialog(DialogsId.WeeklyOff, {
                            luisdata: onTurnProperty,
                            dialogidreset: true,
                            id: DialogsId.ProvideWeeklyOff
                        } );

                    case 'HRQnaIntent':
                    case 'TIMQnAIntent':
                    case 'IDEASQnAIntent':
                    case 'FinanceQnAIntent':
                    case 'CSQnAIntent':
                        return await stepContext.beginDialog('QnADialog',
                            { luisdata: {
                                entities: []
                            },
                            dialogidreset: false});
                              

                        // Intent API's Begin

                        // LMS Intent API
                    case 'LMS_Leave_MandatoryLeaves_Information':
                        return await stepContext.beginDialog('QnADialog',
                            { luisdata: {
                                entities: []
                            },
                            dialogidreset: false,
                            intentApiName: IntentApi.LMS_Leave_MandatoryLeaves_Information
                            });

                    case 'LMS_Leave_LeaveBalance_Information':
                        return await stepContext.beginDialog('QnADialog',
                            { luisdata: {
                                entities: []
                            },
                            dialogidreset: false,
                            intentApiName: IntentApi.LMS_Leave_LeaveBalance_Information});

                    case 'LMS_Leave_LeaveHistory_Information':
                        return await stepContext.beginDialog('QnADialog',
                            { luisdata: {
                                entities: []
                            },
                            dialogidreset: false,
                            intentApiName: IntentApi.LMS_Leave_LeaveHistory_Information});

                    case 'LMS_ChildAdoption_Entitlement':
                        return await stepContext.beginDialog('QnADialog',
                            { luisdata: {
                                entities: []
                            },
                            dialogidreset: false,
                            intentApiName: IntentApi.LMS_ChildAdoption_Entitlement});

                    case 'LMS_Leave_Bereavement_Entitlement':
                        return await stepContext.beginDialog('QnADialog',
                            { luisdata: {
                                entities: []
                            },
                            dialogidreset: false,
                            intentApiName: IntentApi.LMS_Leave_Bereavement_Entitlement});

                    case 'LMS_Leave_Hospitaliztion_Entitlement':
                        return await stepContext.beginDialog('QnADialog',
                            { luisdata: {
                                entities: []
                            },
                            dialogidreset: false,
                            intentApiName: IntentApi.LMS_Leave_Hospitaliztion_Entitlement});

                    case 'LMS_Leave_OptionalHoliday_Information':
                        return await stepContext.beginDialog('QnADialog',
                            { luisdata: {
                                entities: []
                            },
                            dialogidreset: false,
                            intentApiName: IntentApi.LMS_Leave_OptionalHoliday_Information});

                    case 'LMS_Leave_NextOptionalHoliday_Information':
                        return await stepContext.beginDialog('QnADialog',
                            { luisdata: {
                                entities: []
                            },
                            dialogidreset: false,
                            intentApiName: IntentApi.LMS_Leave_NextOptionalHoliday_Information});

                    case 'LMS_HowToApplyLeaveVideo':
                        return await stepContext.beginDialog('QnADialog',
                            { luisdata: {
                                entities: []
                            },
                            dialogidreset: false,
                            intentApiName: IntentApi.LMS_HowToApplyLeaveVideo});
                    
                        // Payroll intent API

                        
                    case 'PYRL_AmtForHoliday_Information' : 
                        return await stepContext.beginDialog('QnADialog',
                            { luisdata: {
                                entities: []
                            },
                            dialogidreset: false,
                            intentApiName: IntentApi.PYRL_AmtForHoliday_Information
                            });
                    case 'PYRL_CompanyTransport_Entitlement' : 
                        return await stepContext.beginDialog('QnADialog',
                            { luisdata: {
                                entities: []
                            },
                            dialogidreset: false,
                            intentApiName: IntentApi.PYRL_CompanyTransport_Entitlement
                            });
                    case 'PYRL_CarRental_Details' : 
                        return await stepContext.beginDialog('QnADialog',
                            { luisdata: {
                                entities: []
                            },
                            dialogidreset: false,
                            intentApiName: IntentApi.PYRL_CarRental_Details
                            });
                    case 'PYRL_DriverSalary_Entitlement' : 
                        return await stepContext.beginDialog('QnADialog',
                            { luisdata: {
                                entities: []
                            },
                            dialogidreset: false,
                            intentApiName: IntentApi.PYRL_DriverSalary_Entitlement
                            });
                    case 'PYRL_FuelMaintanance_Details' : 
                        return await stepContext.beginDialog('QnADialog',
                            { luisdata: {
                                entities: []
                            },
                            dialogidreset: false,
                            intentApiName: IntentApi.PYRL_FuelMaintanance_Details
                            });
                    case 'PYRL_HealthCheckup' : 
                        return await stepContext.beginDialog('QnADialog',
                            { luisdata: {
                                entities: []
                            },
                            dialogidreset: false,
                            intentApiName: IntentApi.PYRL_HealthCheckup
                            });
                    case 'PYRL_HealthCheckupStatus' : 
                        return await stepContext.beginDialog('QnADialog',
                            { luisdata: {
                                entities: []
                            },
                            dialogidreset: false,
                            intentApiName: IntentApi.PYRL_HealthCheckupStatus
                            });
                    case 'PYRL_LifeInsurance_Entitlement' : 
                        return await stepContext.beginDialog('QnADialog',
                            { luisdata: {
                                entities: []
                            },
                            dialogidreset: false,
                            intentApiName: IntentApi.PYRL_LifeInsurance_Entitlement
                            });
                    case 'PYRL_LoanForHouseRentDeposit' : 
                        return await stepContext.beginDialog('QnADialog',
                            { luisdata: {
                                entities: []
                            },
                            dialogidreset: false,
                            intentApiName: IntentApi.PYRL_LoanForHouseRentDeposit
                            });
                    case 'PYRL_LocalConveyance_Entitlement' : 
                        return await stepContext.beginDialog('QnADialog',
                            { luisdata: {
                                entities: []
                            },
                            dialogidreset: false,
                            intentApiName: IntentApi.PYRL_LocalConveyance_Entitlement
                            });
                    case 'PYRL_LTA_Details' : 
                        return await stepContext.beginDialog('QnADialog',
                            { luisdata: {
                                entities: []
                            },
                            dialogidreset: false,
                            intentApiName: IntentApi.PYRL_LTA_Details
                            });
                    case 'PYRL_Medical_Details' : 
                        return await stepContext.beginDialog('QnADialog',
                            { luisdata: {
                                entities: []
                            },
                            dialogidreset: false,
                            intentApiName: IntentApi.PYRL_Medical_Details
                            });
                    case 'PYRL_MedicalInsurance_Entitlement' : 
                        return await stepContext.beginDialog('QnADialog',
                            { luisdata: {
                                entities: []
                            },
                            dialogidreset: false,
                            intentApiName: IntentApi.PYRL_MedicalInsurance_Entitlement
                            });
                    case 'PYRL_Miscarriage_Entitlement' : 
                        return await stepContext.beginDialog('QnADialog',
                            { luisdata: {
                                entities: []
                            },
                            dialogidreset: false,
                            intentApiName: IntentApi.PYRL_Miscarriage_Entitlement
                            });
                    case 'PYRL_Mobile_Entitlement' : 
                        return await stepContext.beginDialog('QnADialog',
                            { luisdata: {
                                entities: []
                            },
                            dialogidreset: false,
                            intentApiName: IntentApi.PYRL_Mobile_Entitlement
                            });
                    case 'PYRL_MoboMeal_Details' : 
                        return await stepContext.beginDialog('QnADialog',
                            { luisdata: {
                                entities: []
                            },
                            dialogidreset: false,
                            intentApiName: IntentApi.PYRL_MoboMeal_Details
                            });
                    case 'PYRL_PersonalAccidentInsurance_Entitlement' : 
                        return await stepContext.beginDialog('QnADialog',
                            { luisdata: {
                                entities: []
                            },
                            dialogidreset: false,
                            intentApiName: IntentApi.PYRL_PersonalAccidentInsurance_Entitlement
                            });
                    case 'PYRL_Relocation_Entitlement' : 
                        return await stepContext.beginDialog('QnADialog',
                            { luisdata: {
                                entities: []
                            },
                            dialogidreset: false,
                            intentApiName: IntentApi.PYRL_Relocation_Entitlement
                            });
                    case 'PYRL_SalaryAdvance_Entitlement' : 
                        return await stepContext.beginDialog('QnADialog',
                            { luisdata: {
                                entities: []
                            },
                            dialogidreset: false,
                            intentApiName: IntentApi.PYRL_SalaryAdvance_Entitlement
                            });
                    case 'PYRL_SuperAnnuation_Entitlement' : 
                        return await stepContext.beginDialog('QnADialog',
                            { luisdata: {
                                entities: []
                            },
                            dialogidreset: false,
                            intentApiName: IntentApi.PYRL_SuperAnnuation_Entitlement
                            });
                    case 'PYRL_Telephone_Details' : 
                        return await stepContext.beginDialog('QnADialog',
                            { luisdata: {
                                entities: []
                            },
                            dialogidreset: false,
                            intentApiName: IntentApi.PYRL_Telephone_Details
                            });
                    case 'PYRL_HowToAccessCompRevLetter_Video' : 
                        return await stepContext.beginDialog('QnADialog',
                            { luisdata: {
                                entities: []
                            },
                            dialogidreset: false,
                            intentApiName: IntentApi.PYRL_HowToAccessCompRevLetter_Video
                            });
                    case 'PYRL_HowToGetForm16_Video' : 
                        return await stepContext.beginDialog('QnADialog',
                            { luisdata: {
                                entities: []
                            },
                            dialogidreset: false,
                            intentApiName: IntentApi.PYRL_HowToGetForm16_Video
                            });
                    case 'PYRL_HowToITDeclaration_Video' : 
                        return await stepContext.beginDialog('QnADialog',
                            { luisdata: {
                                entities: []
                            },
                            dialogidreset: false,
                            intentApiName: IntentApi.PYRL_HowToITDeclaration_Video
                            });
                    case 'PYRL_OnCallllowance_Details' : 
                        return await stepContext.beginDialog('QnADialog',
                            { luisdata: {
                                entities: []
                            },
                            dialogidreset: false,
                            intentApiName: IntentApi.PYRL_OnCallllowance_Details
                            });

                        // Personal deatils intent api
                    case 'PRSNL_ContactNumber_Information' : 
                        return await stepContext.beginDialog('QnADialog',
                            { luisdata: {
                                entities: []
                            },
                            dialogidreset: false,
                            intentApiName: IntentApi.PRSNL_ContactNumber_Information
                            });
                    case 'PRSNL_Country_Information' : 
                        return await stepContext.beginDialog('QnADialog',
                            { luisdata: {
                                entities: []
                            },
                            dialogidreset: false,
                            intentApiName: IntentApi.PRSNL_Country_Information
                            });
                    case 'PRSNL_Designation' : 
                        return await stepContext.beginDialog('QnADialog',
                            { luisdata: {
                                entities: []
                            },
                            dialogidreset: false,
                            intentApiName: IntentApi.PRSNL_Designation
                            });
                    case 'PRSNL_Location_Details' : 
                        return await stepContext.beginDialog('QnADialog',
                            { luisdata: {
                                entities: []
                            },
                            dialogidreset: false,
                            intentApiName: IntentApi.PRSNL_Location_Details
                            });
                    case 'PRSNL_Band_Details' : 
                        return await stepContext.beginDialog('QnADialog',
                            { luisdata: {
                                entities: []
                            },
                            dialogidreset: false,
                            intentApiName: IntentApi.PRSNL_Band_Details
                            });
                    case 'PRSNL_MyName' : 
                        return await stepContext.beginDialog('QnADialog',
                            { luisdata: {
                                entities: []
                            },
                            dialogidreset: false,
                            intentApiName: IntentApi.PRSNL_MyName
                            });
                    case 'PRSNL_HowToActivateUANNumber_Video' : 
                        return await stepContext.beginDialog('QnADialog',
                            { luisdata: {
                                entities: []
                            },
                            dialogidreset: false,
                            intentApiName: IntentApi.PRSNL_HowToActivateUANNumber_Video
                            });
                    case 'PRSNL_HowToChangeFromCurrentRMToNewRM_Video' : 
                        return await stepContext.beginDialog('QnADialog',
                            { luisdata: {
                                entities: []
                            },
                            dialogidreset: false,
                            intentApiName: IntentApi.PRSNL_HowToChangeFromCurrentRMToNewRM_Video
                            });
                    case 'PRSNL_HowToCreateBlogOnMB_Video' : 
                        return await stepContext.beginDialog('QnADialog',
                            { luisdata: {
                                entities: []
                            },
                            dialogidreset: false,
                            intentApiName: IntentApi.PRSNL_HowToCreateBlogOnMB_Video
                            });
                    case 'PRSNL_HowToCreateCommunityOnMB_Video' : 
                        return await stepContext.beginDialog('QnADialog',
                            { luisdata: {
                                entities: []
                            },
                            dialogidreset: false,
                            intentApiName: IntentApi.PRSNL_HowToCreateCommunityOnMB_Video
                            });
                    case 'PRSNL_HowToEditProfileOnMB_Video' : 
                        return await stepContext.beginDialog('QnADialog',
                            { luisdata: {
                                entities: []
                            },
                            dialogidreset: false,
                            intentApiName: IntentApi.PRSNL_HowToEditProfileOnMB_Video
                            });
                    case 'PRSNL_HowToEditBlogOnMB_Video' : 
                        return await stepContext.beginDialog('QnADialog',
                            { luisdata: {
                                entities: []
                            },
                            dialogidreset: false,
                            intentApiName: IntentApi.PRSNL_HowToEditBlogOnMB_Video
                            });
                    case 'PRSNL_HowToEncashKudosPoints_Video' : 
                        return await stepContext.beginDialog('QnADialog',
                            { luisdata: {
                                entities: []
                            },
                            dialogidreset: false,
                            intentApiName: IntentApi.PRSNL_HowToEncashKudosPoints_Video
                            });
                    case 'PRSNL_HowToJoinCommunityMB_Video' : 
                        return await stepContext.beginDialog('QnADialog',
                            { luisdata: {
                                entities: []
                            },
                            dialogidreset: false,
                            intentApiName: IntentApi.PRSNL_HowToJoinCommunityMB_Video
                            });
                    case 'PRSNL_HowToUpdateBankDetailsinPACEHR' : 
                        return await stepContext.beginDialog('QnADialog',
                            { luisdata: {
                                entities: []
                            },
                            dialogidreset: false,
                            intentApiName: IntentApi.PRSNL_HowToUpdateBankDetailsinPACEHR
                            });
                    case 'PRSNL_HowToUpdateBasicPrsnlInfoInMBPlus_Video' : 
                        return await stepContext.beginDialog('QnADialog',
                            { luisdata: {
                                entities: []
                            },
                            dialogidreset: false,
                            intentApiName: IntentApi.PRSNL_HowToUpdateBasicPrsnlInfoInMBPlus_Video
                            });
                    case 'PRSNL_HowToUpdateMobileNumberOnUANPortal_Video' : 
                        return await stepContext.beginDialog('QnADialog',
                            { luisdata: {
                                entities: []
                            },
                            dialogidreset: false,
                            intentApiName: IntentApi.PRSNL_HowToUpdateMobileNumberOnUANPortal_Video
                            });
                    case 'PRSNL_HowToUpdatemyERABankDetails_Video' : 
                        return await stepContext.beginDialog('QnADialog',
                            { luisdata: {
                                entities: []
                            },
                            dialogidreset: false,
                            intentApiName: IntentApi.PRSNL_HowToUpdatemyERABankDetails_Video
                            });

                        // Expense intent API
                    case 'EXPN_HowToClaimRelocationAllowance_Video' :  
                        return await stepContext.beginDialog('QnADialog',
                            { luisdata: {
                                entities: []
                            },
                            dialogidreset: false,
                            intentApiName: IntentApi.EXPN_HowToClaimRelocationAllowance_Video
                            });
                    case 'EXPN_HowToModifyExpenseReport_Video' :  
                        return await stepContext.beginDialog('QnADialog',
                            { luisdata: {
                                entities: []
                            },
                            dialogidreset: false,
                            intentApiName: IntentApi.EXPN_HowToModifyExpenseReport_Video
                            });
                    case 'EXPN_HowToRaiseVisaRelatedClaim_Video' :  
                        return await stepContext.beginDialog('QnADialog',
                            { luisdata: {
                                entities: []
                            },
                            dialogidreset: false,
                            intentApiName: IntentApi.EXPN_HowToRaiseVisaRelatedClaim_Video
                            });
                    case 'EXPN_HowToReumburseYourChildSchoolFee_Video' :  
                        return await stepContext.beginDialog('QnADialog',
                            { luisdata: {
                                entities: []
                            },
                            dialogidreset: false,
                            intentApiName: IntentApi.EXPN_HowToReumburseYourChildSchoolFee_Video
                            });
                    case 'EXPN_HowToViewExpenseReport_Video' :  
                        return await stepContext.beginDialog('QnADialog',
                            { luisdata: {
                                entities: []
                            },
                            dialogidreset: false,
                            intentApiName: IntentApi.EXPN_HowToViewExpenseReport_Video
                            });
                    case 'EXPN_HowToSubmitExpsenseClaim_Video' :  
                        return await stepContext.beginDialog('QnADialog',
                            { luisdata: {
                                entities: []
                            },
                            dialogidreset: false,
                            intentApiName: IntentApi.EXPN_HowToSubmitExpsenseClaim_Video
                            });
                    case 'EXPN_HowToPrintExpenseReport_Video' :  
                        return await stepContext.beginDialog('QnADialog',
                            { luisdata: {
                                entities: []
                            },
                            dialogidreset: false,
                            intentApiName: IntentApi.EXPN_HowToPrintExpenseReport_Video
                            });
                    case 'EXPN_HowToEnterModifyExpenseReport_Video' :  
                        return await stepContext.beginDialog('QnADialog',
                            { luisdata: {
                                entities: []
                            },
                            dialogidreset: false,
                            intentApiName: IntentApi.EXPN_HowToEnterModifyExpenseReport_Video
                            });
                    case 'EXPN_HowToDeleteExpenseClaim_Video' :  
                        return await stepContext.beginDialog('QnADialog',
                            { luisdata: {
                                entities: []
                            },
                            dialogidreset: false,
                            intentApiName: IntentApi.EXPN_HowToDeleteExpenseClaim_Video
                            });

                        // Cash advance video
                    case 'CADV_HowToEnterModifyCashAdvance_Video' :   
                        return await stepContext.beginDialog('QnADialog',
                            { luisdata: {
                                entities: []
                            },
                            dialogidreset: false,
                            intentApiName: IntentApi.CADV_HowToEnterModifyCashAdvance_Video
                            });
                    case 'CADV_HowToRaiseReqForBusinessTravelCashAdv_Video' :   
                        return await stepContext.beginDialog('QnADialog',
                            { luisdata: {
                                entities: []
                            },
                            dialogidreset: false,
                            intentApiName: IntentApi.CADV_HowToRaiseReqForBusinessTravelCashAdv_Video
                            });
                    case 'CADV_HowToViewCashAdvanceDetails_Video' :   
                        return await stepContext.beginDialog('QnADialog',
                            { luisdata: {
                                entities: []
                            },
                            dialogidreset: false,
                            intentApiName: IntentApi.CADV_HowToViewCashAdvanceDetails_Video
                            });

                    // Shift allowance API
                    case 'SA_GetShiftAllowance_Details': 
                        return await stepContext.beginDialog('QnADialog',
                            { luisdata: {
                                entities: []
                            },
                            dialogidreset: false,
                            intentApiName: IntentApi.SA_GetShiftAllowance_Details
                            });
                    
                        // Travel API
                    case 'TRVL_HowToReopenTravelRequest_Video': 
                        return await stepContext.beginDialog('QnADialog',
                            { luisdata: {
                                entities: []
                            },
                            dialogidreset: false,
                            intentApiName: IntentApi.TRVL_HowToReopenTravelRequest_Video
                            });
                    // CS LUIS API
                    case 'CS_HowToApplyForParkingSticker_Video' : 
                        return await stepContext.beginDialog('QnADialog',
                            { luisdata: {
                                entities: []
                            },
                            dialogidreset: false,
                            intentApiName: IntentApi.CS_HowToApplyForParkingSticker_Video
                            });
                    case 'Medi_HowToGetMedibuddyMedicalCard_Video' : 
                        return await stepContext.beginDialog('QnADialog',
                            { luisdata: {
                                entities: []
                            },
                            dialogidreset: false,
                            intentApiName: IntentApi.Medi_HowToGetMedibuddyMedicalCard_Video
                            });
                    case 'Medi_HowToLoginIntoMedibuddyPortal_Video' : 
                        return await stepContext.beginDialog('QnADialog',
                            { luisdata: {
                                entities: []
                            },
                            dialogidreset: false,
                            intentApiName: IntentApi.Medi_HowToLoginIntoMedibuddyPortal_Video
                            });


                        // Intent API's END
                    
                    case 'CARE_Transactions_WorkHarassment':
                    case 'CARE_other':
                    case 'CARE_Transactions_InterpersonalIssue':
                    case 'CARE_Transactions_PolicyViolation':
                        return await stepContext.beginDialog(DialogsId.CARE, {
                            luisdata: onTurnProperty,
                            dialogidreset: false
                        } );
                    
                    // Finance / Procurement
                    case 'PROC_PafStatus':
                        return await stepContext.beginDialog(DialogsId.FinancePAFStatus, {
                            luisdata: onTurnProperty,
                            dialogidreset: false
                        } );
                    // Pistatus
                    case 'PROC_Pistatus':
                        return await stepContext.beginDialog(DialogsId.FinancePIStatus,{
                            luisdata: onTurnProperty,
                            dialogidreset: false
                        });

                    // DER Modules
                    case 'DER_Submit_Transactions':
                        return await stepContext.beginDialog(DialogsId.SubmitDer,{
                            luisdata: onTurnProperty,
                            dialogidreset: false
                        });
                    case 'DER_Transactions':
                        return await stepContext.beginDialog(DialogsId.ApproveDer,{
                            luisdata: onTurnProperty,
                            dialogidreset: false
                        });

                    // MER Modules
                    case 'MER_Transactions':
                        //TODO
                        return await stepContext.beginDialog(DialogsId.MerApproval,{
                            luisdata: onTurnProperty,
                            dialogidreset: false
                        });
                    case 'MER_Submit_Transactions':
                        return await stepContext.beginDialog(DialogsId.SubmitMer,{
                            luisdata: onTurnProperty,
                            dialogidreset: false
                        });
                    case 'TibotIntent':
                        return await stepContext.beginDialog(DialogsId.TiBot,{
                            luisdata: onTurnProperty,
                            dialogidreset: false
                        });
                    // Book my seat
                    case 'CS_BookMySeat':
                        return await stepContext.beginDialog(DialogsId.BookMySeat,{
                            luisdata: onTurnProperty,
                            dialogidreset: false
                        });
                    default:
                        this.fallback = true;
                        return await stepContext.beginDialog('QnADialog',
                            { luisdata: {
                                entities: []
                            },
                            dialogidreset: true,
                            id: 'QnADefault'
                            });
                    }          
                } else {
                    uvoLogger.logFailedUttToDB(stepContext.context, userData);
                    switch (stepContext.context.activity.text) {
                    default:
                        this.fallback = true;
                        return await stepContext.beginDialog('QnADialog',
                            { luisdata: {
                                entities: []
                            },
                            dialogidreset: true,
                            id: 'QnADefault'
                            });
                    }
                }
            }
        } catch (err) {
            await stepContext.context.sendActivity(`I didn't quiet catch that, please try again.`);
            // console.log('>>>>>>>>>>>>> line 757',await stepContext.continueDialog());
            console.log('Main Error Handler',err);
            uvoLogger.errorLogger(stepContext.context, userData, err);
            await stepContext.cancelAllDialogs();
            return await stepContext.endDialog();
        }
    }

    async finalStep(stepContext) {

        const dialogoptions = stepContext.result;

        if(dialogoptions && dialogoptions.forLuis ){
            await stepContext.cancelAllDialogs();
            stepContext.context.activity.text = dialogoptions.userQuery;
            let luisData = await LuisData(this.recognizer, stepContext);
            return await stepContext.replaceDialog(this.id, { forLuis: true, luisData });
        }else{
            console.log('third step main dispatcher');
            return await stepContext.cancelAllDialogs();
        }

    }

    async onContinueDialog(innerDc) {
        const result = await this.interrupt(innerDc);
        if (result) {
            return result;
        }
        return await super.onContinueDialog(innerDc);
    }

    async interrupt(innerDc) {
        if (innerDc.context.activity.text) {
            const text = innerDc.context.activity.text.trim().toLowerCase();
            let interruptlist = ['help', '?', 'exit', 'cancel', 'quit', 'bye'];
            if (interruptlist.includes(text)) {
                console.log('interrupt found');
                switch (text) {
                case 'help':
                case '?': {
                    /**
                         * Need to update
                         */
                    await innerDc.context.sendActivity(strformat(BotReplies.help, userData.Name));
                    return await innerDc.endDialog();
                }
                case 'exit':
                case 'bye':
                case 'cancel':
                case 'quit': {
                    await innerDc.context.sendActivity(BotReplies.signoff);
                    return await innerDc.cancelAllDialogs();
                }
                }
            } else {
                // let onTurnProperty = await LuisData(this.recognizer, innerDc);
                // console.log('interrupt not found else', onTurnProperty);
                // if(onTurnProperty && onTurnProperty.intent !== '' && onTurnProperty.intent !== 'None') {
                //     console.log('interrupt not found else with luis', onTurnProperty);
                //     return await innerDc.replaceDialog(this.id, {
                //         interrupt: true,
                //         luisData: onTurnProperty
                //     });
                // } else {
                //     console.log('interrupt not found else return null', onTurnProperty);
                //     return null;
                // }
                return null;
            }


        }

    }
}


async function LuisData(recognizer, stepContext) {
    return new Promise(async (resolve, reject) => {
        try {
            //Dev execution time log
            console.log('Call to Main Dispatcher initiated at ',new Date());
            //Dev execution time log

            let mainDispatchRes = await recognizer.recognize(stepContext.context);
            let mainDispatchIntent = OnTurnProperty.fromLUISResults(mainDispatchRes, null);
            let onTurnProperty = {};
            let subrecog;
            let subrecognizer;
            console.log('mainDispatchRes',JSON.stringify(mainDispatchRes));
            console.log('mainDispatchIntent --------------------', mainDispatchIntent);
            switch (mainDispatchIntent.intent) {
            case 'UVO-v4-CSDispatcher':
                subrecognizer = new LuisRecognizer({
                    applicationId: configEngine.getEnv(config.AzureServices.Luis.CSDispatcher.LuisAppId),
                    endpointKey: configEngine.getEnv(config.AzureServices.Luis.CSDispatcher.LuisAPIKey),
                    endpoint: configEngine.getEnv(config.AzureServices.Luis.CSDispatcher.LuisAPIHostName)
                },
                {
                    apiVersion: 'v3'
                });

                subrecog = await subrecognizer.recognize(stepContext.context);
                console.log('subrecog -------------------',JSON.stringify(subrecog));

                onTurnProperty = OnTurnProperty.fromLUISResults(subrecog, subrecog.luisResult.prediction.intents[Object.keys(subrecog.luisResult.prediction.intents)[0]].childApp);
                break;
            case 'UVO-v4-FinanceDispatcher':

                subrecognizer = new LuisRecognizer({
                    applicationId: configEngine.getEnv(config.AzureServices.Luis.FinanceDispatcher.LuisAppId),
                    endpointKey: configEngine.getEnv(config.AzureServices.Luis.FinanceDispatcher.LuisAPIKey),
                    endpoint: configEngine.getEnv(config.AzureServices.Luis.FinanceDispatcher.LuisAPIHostName)
                },
                {
                    apiVersion: 'v3'
                });
                subrecog = await subrecognizer.recognize(stepContext.context);
                console.log('subrecog',JSON.stringify(subrecog));

                onTurnProperty = OnTurnProperty.fromLUISResults(subrecog, subrecog.luisResult.prediction.intents[Object.keys(subrecog.luisResult.prediction.intents)[0]].childApp);
                break;
            case 'UVO-v4-HRDispatcher':
                subrecognizer = new LuisRecognizer({
                    applicationId: configEngine.getEnv(config.AzureServices.Luis.HRDispatcher.LuisAppId),
                    endpointKey: configEngine.getEnv(config.AzureServices.Luis.HRDispatcher.LuisAPIKey),
                    endpoint: configEngine.getEnv(config.AzureServices.Luis.HRDispatcher.LuisAPIHostName)
                },
                {
                    apiVersion: 'v3'
                });
                subrecog = await subrecognizer.recognize(stepContext.context);
                console.log('subrecog',JSON.stringify(subrecog));

                onTurnProperty = OnTurnProperty.fromLUISResults(subrecog, subrecog.luisResult.prediction.intents[Object.keys(subrecog.luisResult.prediction.intents)[0]].childApp);
                break;
            case 'UVO-v4-IDEASDispatcher':
                subrecognizer = new LuisRecognizer({
                    applicationId: configEngine.getEnv(config.AzureServices.Luis.IDEASDispatcher.LuisAppId),
                    endpointKey: configEngine.getEnv(config.AzureServices.Luis.IDEASDispatcher.LuisAPIKey),
                    endpoint: configEngine.getEnv(config.AzureServices.Luis.IDEASDispatcher.LuisAPIHostName)
                },
                {
                    apiVersion: 'v3'
                });
                subrecog = await subrecognizer.recognize(stepContext.context);
                console.log('subrecog',JSON.stringify(subrecog));

                onTurnProperty = OnTurnProperty.fromLUISResults(subrecog, subrecog.luisResult.prediction.intents[Object.keys(subrecog.luisResult.prediction.intents)[0]].childApp);

                break;
            case 'UVO-v4-TIMDispatcher':
                subrecognizer = new LuisRecognizer({
                    applicationId: configEngine.getEnv(config.AzureServices.Luis.TIMDispatcher.LuisAppId),
                    endpointKey: configEngine.getEnv(config.AzureServices.Luis.TIMDispatcher.LuisAPIKey),
                    endpoint: configEngine.getEnv(config.AzureServices.Luis.TIMDispatcher.LuisAPIHostName)
                },
                {
                    apiVersion: 'v3'
                });
                subrecog = await subrecognizer.recognize(stepContext.context);
                console.log('subrecog',JSON.stringify(subrecog));

                onTurnProperty = OnTurnProperty.fromLUISResults(subrecog, subrecog.luisResult.prediction.intents[Object.keys(subrecog.luisResult.prediction.intents)[0]].childApp);

                break;
            case 'UVO-CommonLuis':
                subrecognizer = new LuisRecognizer({
                    applicationId: configEngine.getEnv(config.AzureServices.Luis.CommonLuis.LuisAppId),
                    endpointKey: configEngine.getEnv(config.AzureServices.Luis.CommonLuis.LuisAPIKey),
                    endpoint: configEngine.getEnv(config.AzureServices.Luis.CommonLuis.LuisAPIHostName)
                },
                {
                    apiVersion: 'v3'
                });
                subrecog = await subrecognizer.recognize(stepContext.context);
                console.log('uvo-CommonLuis--------------', subrecog);
                onTurnProperty = OnTurnProperty.fromLUISResults(subrecog, null);
                break;
            default:
                console.log('None Found');
                break;
            }
            console.log('Final result after making sub-dispatcher call received at :', new Date());
            resolve(onTurnProperty);
        } catch (err) {
            console.log(err);
        }

    });
}

module.exports.MainDispatcher = MainDispatcher;